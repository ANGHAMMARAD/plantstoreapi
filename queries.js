const { 
    sequelize,
    Op,
    Check_Row,
    Client,
    Composition,
    Composition_photo,
    Composition_Plant,
    Lightning,
    Lightning_has_Plant,
    Order,
    Plant,
    Plant_has_Wanted_Soil,
    Plant_in_Pot,
    Plant_Photo,
    Plant_Type,
    Pot_Size,
    User,
    Wanted_Soil,
    Watering,
} = require('./sequelize_init')

async function getItem(id) {
    let item = await Plant.findOne({
      where: {
        ID_Plant: id
      }
    }).then(item => {
      if (item == null) return null;
      return {
        id: item.ID_Plant,
        name: item.Ukr_name,
        lname: item.Latin_name,
        height: item.Average_Height,
        description: item.Description,
        grade: item._Average_Grade,
        waterId: item.ID_Watering,
      }
    })
    if (item == null) return null;
    item.photos = await Plant_Photo.findAll({
      where: {
        ID_Plant: item.id
      }
    }).then(photos => photos.map(photo => photo.Photo_Link))
  
    item.light = await Lightning_has_Plant.findAll({
      where: {
        Plant_ID_Plant: item.id
      }
    }).then(light => light.map(elem => elem.Lightning_ID_Lightning))
  
    item.soil = await Plant_has_Wanted_Soil.findAll({
      where: {
        Plant_ID_Plant: item.id
      }
    }).then(soil => soil.map(elem => elem.Wanted_Soil_ID_Soil_Type))
  
    item.compositions = await Composition_Plant.findAll({
      where: {
        ID_Plant: item.id
      }
    }).then(list => list.map(elem => elem.ID_Composition))
  
    item.sizes = await Plant_in_Pot.findAll({
      where: {
        ID_Plant: item.id
      }
    }).then(list => list.map(elem => {
      return {
        id: elem.ID_Pot,
        amount: elem.Quantity,
        price: elem.Price
      }
    }))
    return item;
  }

  async function getItems() {
    return Plant.findAll().then(items => items.map(
        item => {
          return {
            id: item.ID_Plant,
            name: item.Ukr_name,
            lname: item.Latin_name,
            height: item.Average_Height,
            description: item.Description,
            grade: item._Average_Grade,
            waterId: item.ID_Watering,
          }
        })).map(item => Promise.all([
          Plant_Photo.findAll({
            where: {
              ID_Plant: item.id
            }
          }).then(photos => photos.map(photo => photo.Photo_Link)),
          Lightning_has_Plant.findAll({
            where: {
              Plant_ID_Plant: item.id
            }
          }).then(light => light.map(elem => elem.Lightning_ID_Lightning)),
          Plant_has_Wanted_Soil.findAll({
            where: {
              Plant_ID_Plant: item.id
            }
          }).then(soil => soil.map(elem => elem.Wanted_Soil_ID_Soil_Type)),
          Composition_Plant.findAll({
            where: {
              ID_Plant: item.id
            }
          }).then(list => list.map(elem => elem.ID_Composition)),
          Plant_in_Pot.findAll({
            where: {
              ID_Plant: item.id
            }
          }).then(list => list.map(elem => {
            return {
              id: elem.ID_Pot,
              amount: elem.Quantity,
              price: elem.Price
            }
          }))
        ]).then(values => {
          item.photos = values[0];
          item.light = values[1];
          item.soil = values[2];
          item.compositions = values[3];
          item.sizes = values[4];
          // console.log(item)
          return item;
        })
        )
  }

  async function getItemsQuery(query, light_f, soil_f) {
    return Plant.findAll({where: query}).then(items => items.map(
        item => {
          return {
            id: item.ID_Plant,
            name: item.Ukr_name,
            lname: item.Latin_name,
            height: item.Average_Height,
            description: item.Description,
            grade: item._Average_Grade,
            waterId: item.ID_Watering,
          }
        }))
        // .then(plants => {
        //   light_f.forEach(light_id => {

        //     Lightning_has_Plant.findAll({
        //       where: {
        //         Lightning_ID_Lightning: light_id
        //       }
        //     })
        //   })
          
        // })
        .map(item => Promise.all([
          Plant_Photo.findAll({
            where: {
              ID_Plant: item.id
            }
          }).then(photos => photos.map(photo => photo.Photo_Link)),
          Lightning_has_Plant.findAll({
            where: {
              Plant_ID_Plant: item.id
            }
          }).then(light => {
            return light.map(elem => elem.Lightning_ID_Lightning)
          }),
          Plant_has_Wanted_Soil.findAll({
            where: {
              Plant_ID_Plant: item.id
            }
          }
        ).then(soil => {
          return soil.map(elem => elem.Wanted_Soil_ID_Soil_Type)
        }),
          Composition_Plant.findAll({
            where: {
              ID_Plant: item.id
            }
          }).then(list => list.map(elem => elem.ID_Composition)),
          Plant_in_Pot.findAll({
            where: {
              ID_Plant: item.id
            }
          }).then(list => list.map(elem => {
            return {
              id: elem.ID_Pot,
              amount: elem.Quantity,
              price: elem.Price
            }
          }))
        ]).then(values => {
          item.photos = values[0];
          item.light = values[1];
          item.soil = values[2];
          item.compositions = values[3];
          item.sizes = values[4];
          // console.log(item)
          return item;
        })
        )
  }

  async function getCompositions() {
    return Composition.findAll()
      .then(comps => comps.map(
        elem => {
          return {
            id_comp: elem.ID_Composition,
            name: elem.Name
          }
        }
      )).map(comp => {
  
        return Promise.all([
          Composition_photo.findAll({
            where: {
              ID_Composition: comp.id_comp
            }
          }).then(photos => photos.map(photo => photo.Photo_Link)),
          Composition_Plant.findAll({
            where: {
              ID_Composition: comp.id_comp
            }
          }).then(c_ps => c_ps.map(c_p => c_p.ID_Plant)),
  
        ]).then(values => {
          comp.photos = values[0]
          comp.plants = values[1]
          return comp;
        }
        ).then(comp => {
          let [sum, amount] = [0, 0];
          comp.plants.forEach(elem => {
            sum += elem._Average_Grade
            amount += elem._Grades_Quantity
          })
          comp.rating = (amount !== 0 && sum > 0) ? parseFloat(sum / amount) : null;
          return comp;
        })
      })
  
  }

async function getCategories(){
  return Plant_Type.findAll(
    {
      include: [
        {
          model: Plant,
          required: true
        }
      ]
    }
  )
  .map((type) => {
    let plants = type.Plants.map(plant => plant.ID_Plant)
    return {
      id: type.ID_Plant_Type,
      type: type.Description,
      plants: plants
    }
  })
}

exports.getItems= getItems;
exports.getItemsQuery = getItemsQuery;
exports.getItem= getItem;
exports.getCompositions= getCompositions;
exports.getCategories = getCategories;
