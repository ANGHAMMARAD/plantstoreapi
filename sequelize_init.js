const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const Check_Row = require('./models/Check_Row')
const Client = require('./models/Client')
const Composition = require('./models/Composition')
const Composition_photo = require('./models/Composition_photo')
const Composition_Plant = require('./models/Composition_Plant')
const Lightning = require('./models/Lightning')
const Lightning_has_Plant = require('./models/Lightning_has_Plant')
const Order = require('./models/Order')
const Plant = require('./models/Plant')
const Plant_has_Wanted_Soil = require('./models/Plant_has_Wanted_Soil')
const Plant_in_Pot = require('./models/Plant_in_Pot')
const Plant_Photo = require('./models/Plant_Photo')
const Plant_Type = require('./models/Plant_Type')
const Pot_Size = require('./models/Pot_Size')
const User = require('./models/User')
const Wanted_Soil = require('./models/Wanted_Soil')
const Watering = require('./models/Watering')
const DataTypes = Sequelize.DataTypes;
const sequelize = new Sequelize(`mysql://${process.env.db_username}:${process.env.db_password}@${process.env.host}:3306/${process.env.db_name}`, {
  define: {
    freezeTableName: true,
    timestamps: false
  }
})
module.exports ={
    sequelize: sequelize,
    Op: Op,
    Check_Row: Check_Row(sequelize, Sequelize.DataTypes),
    Client: Client(sequelize, Sequelize.DataTypes),
    Composition: Composition(sequelize, Sequelize.DataTypes),
    Composition_photo: Composition_photo(sequelize, Sequelize.DataTypes),
    Composition_Plant: Composition_Plant(sequelize, Sequelize.DataTypes),
    Lightning: Lightning(sequelize, Sequelize.DataTypes),
    Lightning_has_Plant: Lightning_has_Plant(sequelize, Sequelize.DataTypes),
    Order: Order(sequelize, Sequelize.DataTypes),
    Plant: Plant(sequelize, Sequelize.DataTypes),
    Plant_has_Wanted_Soil: Plant_has_Wanted_Soil(sequelize, Sequelize.DataTypes),
    Plant_in_Pot: Plant_in_Pot(sequelize, Sequelize.DataTypes),
    Plant_Photo: Plant_Photo(sequelize, Sequelize.DataTypes),
    Plant_Type: Plant_Type(sequelize, Sequelize.DataTypes),
    Pot_Size: Pot_Size(sequelize, Sequelize.DataTypes),
    User: User(sequelize, Sequelize.DataTypes),
    Wanted_Soil: Wanted_Soil(sequelize, Sequelize.DataTypes),
    Watering: Watering(sequelize, Sequelize.DataTypes),
}