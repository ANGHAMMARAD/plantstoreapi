require('dotenv').config();
const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const app = express()
const port = process.env.PORT || 4000;
const bcrypt = require('bcrypt');
const saltRounds = 10;

const query = require('./queries')
const utils = require('./utils')
const { sequelize,
  Op,
  Check_Row,
  Client,
  Composition,
  Composition_photo,
  Composition_Plant,
  Lightning,
  Lightning_has_Plant,
  Order,
  Plant,
  Plant_has_Wanted_Soil,
  Plant_in_Pot,
  Plant_Photo,
  Plant_Type,
  Pot_Size,
  User,
  Wanted_Soil,
  Watering,
} = require('./sequelize_init')


User.belongsTo(Client, { foreignKey: 'ID_Client', sourceKey: 'ID_Client' })
Client.hasOne(User, { foreignKey: 'ID_Client', sourceKey: 'ID_Client' })
Client.hasMany(Order, { foreignKey: 'ID_Client', sourceKey: 'ID_Client' })
Order.belongsTo(Client, { foreignKey: 'ID_Client', sourceKey: 'ID_Client' })
Order.hasMany(Check_Row, { foreignKey: 'ID_order', sourceKey: 'ID_Order' })
Check_Row.belongsTo(Order, { foreignKey: 'ID_order', sourceKey: 'ID_Order' })
Plant_in_Pot.hasMany(Check_Row, { foreignKey: 'ID_plant_in_pot', sourceKey: 'ID_Plant_in_Pot' })
Check_Row.belongsTo(Plant_in_Pot, { foreignKey: 'ID_plant_in_pot', sourceKey: 'ID_Plant_in_Pot' })
Plant_in_Pot.belongsTo(Plant, { foreignKey: 'ID_Plant', targetKey: 'ID_Plant' })
Plant.hasMany(Plant_in_Pot, { foreignKey: 'ID_Plant', sourceKey: 'ID_Plant' })
Plant_in_Pot.belongsTo(Pot_Size, { foreignKey: 'ID_Pot', sourceKey: 'ID_Pot' })
Pot_Size.hasMany(Plant_in_Pot, { foreignKey: 'ID_Pot', sourceKey: 'ID_Pot' })
Plant.belongsTo(Watering, { foreignKey: 'ID_Watering', sourceKey: 'ID_Watering' })
Watering.hasMany(Plant, { foreignKey: 'ID_Watering', sourceKey: 'ID_Watering' })
Plant.hasMany(Plant_Photo, { foreignKey: 'ID_Plant', sourceKey: 'ID_Plant' })
Plant_Photo.belongsTo(Plant, { foreignKey: 'ID_Plant', sourceKey: 'ID_Plant' })
Plant.belongsTo(Plant_Type, { foreignKey: "ID_Plant_Type", sourceKey: "ID_Plant_Type" })
Plant_Type.hasMany(Plant, { foreignKey: "ID_Plant_Type", sourceKey: "ID_Plant_Type" })
app.use(function (req, res, next) {
  res.set('X-Clacks-Overhead', 'GNU Terry Pratchet');
  res.set('Access-Control-Allow-Origin', '*')
  res.set('Access-Control-Allow-Headers', 'Content-Type')
  next();
});

app.use(cors());
// for parsing application/json
app.use(bodyParser.json());

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));


//middleware that checks if JWT token exists and verifies it if it does exist.
//In all future routes, this helps to know if the request is authenticated or not.
app.use(function (req, res, next) {
  // check header or url parameters or post parameters for token
  var token = req.headers['authorization'];
  if (!token) return next(); //if no token, continue

  token = token.replace('Bearer ', '');
  jwt.verify(token, process.env.JWT_SECRET, function (err, user) {
    if (err) {
      console.log(err)
      // return res.status(401).json({
      //   error: true,
      //   message: "Invalid user."
      // });
    } else {
      console.log(user)
      req.user = user; //set the user to req so other routes can use it
      
    }
    next();
  });
});



app.post('/auth/registration', utils.asyncMiddleware(async (req, res) => {
  const phone = req.body.phone;
  const email = req.body.email;
  const pwd = req.body.password;

  console.log(`Registration ${phone} and ${email} and ${pwd}`)
  if (phone == null || pwd == null) {
    return res.status(400).json({
      error: true,
      message: "Phone, Email, Password are required."
    });
  }


  const amount_exist = await Client.findAll({
    where: {
      [Op.or]: {
        phone: phone,
        email: email
      }
    }
  }).then(rows => rows.length)
  console.log(`Amount: ${amount_exist}`)
  if (amount_exist > 0) return res.status(401).json({
    error: true,
    message: "Phone or Password are already exist."
  });
  else {
    bcrypt.hash(pwd, saltRounds, (err, hash) => {
      if (err) console.log(`Error bcrypt: ${err}`)
      Client.create({ phone: phone, email: email, Notes: "" })
        .then(client => User.create(
          {
            ID_Client: client.ID_Client,
            hash: hash,
            isAdmin: false,
          }, err => {
            console.log(err)
            return res.status(500).json({
              error: true,
              message: "Internal error"
            });
          }
        )
        ).then((client) => {
          // client.getOrders().then(orders=>console.log(orders))
          // generate token
          const token = utils.generateToken({ phone: phone, email: email });
          // get basic user details
          const userObj = utils.getCleanUser({ phone: phone, email: email });
          // return the token along with user details
          return res.json({ user: userObj, token });
        })
    })
  }
}));

// validate the user credentials
app.post('/users/signin', (req, res, next) => {
  const phone = req.body.phone;
  const pwd = req.body.password;
  // return 400 status if username/password is not exist
  console.log(`Sign in ${phone} and ${pwd}`)
  if (phone == null || pwd == null) {
    return res.status(400).json({
      error: true,
      message: "Phone and Password required."
    });
  }

  bcrypt.hash(pwd, saltRounds, (err, hash) => {
    if (err) console.log(`Error bcrypt: ${err}`)
    return Client.findOne({
      where: {
        phone: phone
      },
      include: [{
        model: User,
        required: true,
      }]
    }).then(client => {
      if (client == null) return res.status(401).json({
        error: true,
        message: "Phone or Password is Wrong."
      });
      else {
        // console.log(JSON.stringify(client, null, 4))
        bcrypt.compare(pwd, client.User.hash).then(correct => {
          if (correct) {
            // client.getOrders().then(orders=>console.log(JSON.stringify(orders)))
            const token = utils.generateToken(client);
            // get basic user details
            const userObj = utils.getCleanUser(client);
            // return the token along with user details
            return res.json({ user: userObj, token });
          }
          else return res.status(401).json({
            error: true,
            message: "Phone or Password is Wrong."
          });
        })
      }
    }, err => {
      console.log(`Sequelize query error ${err}`)
      next()
    })
  })
});



app.post('/orders', (req, res) => {
  if (req.user) {
    console.log(JSON.stringify(req.body))
    // console.log(req.user)
  }
  else {
    return res.status(401).json({
      error: true,
      message: "Token required."
    });
  }
})

let items = []




let categories = []
let light = []
let watering = []
let soil = []
let sizes = []
let compositions = []


app.get('/api/goods/:id', (req, res) => {
  res.json(items.find(elem => elem.id == req.params.id));
})

app.get('/api/goods', (req, res) => {
  res.json(items)

})

app.post('/api/search', (req, res) => {
  console.log(JSON.stringify(req.body, null, 4));
  query.getItemsQuery({
    [Op.or]: {
      Ukr_name: {
        [Op.like]: `%${req.body.query}%`
      },
      Latin_name: {
        [Op.like]: `%${req.body.query}%`
      },
    }
  },
  req.body.filter.light,
  req.body.filter.soil,
  ).then(plants => res.json(plants))
})

app.get('/api/composition/:id', (req, res) => {
  console.log("Composition request\n" + JSON.stringify(req.params));
  res.json(compositions.find(elem => elem.id_comp == req.params.id));
})

app.get('/api/composition', (req, res) => {
  res.json(compositions);
})

app.get('/api/sizes', (req, res) => {
  res.send(sizes)
})

app.get('/api/lighting', (req, res) => {
  res.send(light)
})

app.get('/api/watering', (req, res) => {
  res.send(watering)
})

app.get('/api/soil', (req, res) => {
  res.send(soil)
})

app.route('/api/type')
  .get((req, res) => {
    res.send(categories);
  })
// .post((req, res) => {
//   //new: {id:0, type:"Type"}
//   categories.push(req.body.new);
//   res.send(categories);
// })

app.get('/api/type/:id', (req, res, next) => {
  if (categories[req.params.id]) {
    res.send(categories[req.params.id]);
  }
  else next();
})

try {
  sequelize.sync().then(() => {
    Promise.all([
      query.getCategories()
      .then((types)=>{
        console.log(JSON.stringify(types, null, 4))
        categories=types;
      }),

      Wanted_Soil.findAll().then((soils) => {
        soil = soils.map((elem) => { return { id: elem.ID_Soil_Type, description: elem.Description } })
      }),
      Watering.findAll().then((water_types) => {
        watering = water_types.map((elem) => { return { id: elem.ID_Watering, description: elem.Description } })
      }),
      Lightning.findAll().then((light_types) => {
        light = light_types.map((elem) => { return { id: elem.ID_Lightning, description: elem.Description } })
      }),
      Pot_Size.findAll().then((pot_sizes) => {
        sizes = pot_sizes.map((elem) => { return { id: elem.ID_Pot, name: elem.Description } })
      }),
      query.getItems().then(fetched_items => { items = fetched_items }),
      query.getCompositions().then(fetched_comps => {
        console.log(fetched_comps)
        compositions = fetched_comps
      }),
    ]).then(() => {
      app.listen(port,
        () => console.log(`Example app listening at http://localhost:${port}`))
    })
  }
  )
}
catch (err) {
  console.log("Connection to db failed " + err);
  app.listen(port,
    () => console.log(`Example app listening at http://localhost:${port}`))
}