/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Plant_Type', {
		ID_Plant_Type: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Description: {
			type: DataTypes.STRING(20),
			allowNull: false
		}
	}, {
		tableName: 'Plant_Type'
	});
};
