/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Pot_Size', {
		ID_Pot: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Description: {
			type: DataTypes.STRING(40),
			allowNull: false,
		}
	}, {
		tableName: 'Pot_Size'
	});
};
