module.exports = function (sequelize, DataTypes) {
    return sequelize.define('User', {
        ID_User: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        ID_Client: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        hash: {
            type: DataTypes.STRING(60),
            allowNull: false
        },
        isAdmin: {
            type: DataTypes.INTEGER(1),
            allowNull: false
        }
    });

}