/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Client', {
		phone: {
			type: DataTypes.CHAR(10),
			allowNull: false
		},
		email: {
			type: DataTypes.STRING(45),
			allowNull: false
		},
		Notes: {
			type: DataTypes.STRING(200),
			allowNull: true
		},
		ID_Client: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		}
	}, {
		tableName: 'Client'
	});
};
