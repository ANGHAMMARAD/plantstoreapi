/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Lightning_has_Plant', {
		Lightning_ID_Lightning: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Lightning',
				key: 'ID_Lightning'
			}
		},
		Plant_ID_Plant: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Plant',
				key: 'ID_Plant'
			}
		}
	}, {
		tableName: 'Lightning_has_Plant'
	});
};
