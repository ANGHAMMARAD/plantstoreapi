/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Plant_Photo', {
		ID_Photo: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Photo_Link: {
			type: DataTypes.STRING(200),
			allowNull: false
		},
		ID_Plant: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Plant',
				key: 'ID_Plant'
			}
		}
	}, {
		tableName: 'Plant_Photo'
	});
};
