/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Plant', {
		ID_Plant: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Ukr_name: {
			type: DataTypes.STRING(20),
			allowNull: false
		},
		Latin_name: {
			type: DataTypes.STRING(20),
			allowNull: true
		},
		Average_Height: {
			type: DataTypes.FLOAT,
			allowNull: false
		},
		Notes: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		Description: {
			type: DataTypes.STRING(300),
			allowNull: false
		},
		ID_Plant_Type: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Plant_Type',
				key: 'ID_Plant_Type'
			}
		},
		ID_Watering: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Watering',
				key: 'ID_Watering'
			}
		},
		_Grades_Quantity: {
			type: DataTypes.INTEGER(6),
			allowNull: false
		},
		_Sold_Quantity: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			defaultValue: '0'
		},
		_Quantity: {
			type: DataTypes.INTEGER(6),
			allowNull: false
		},
		_Average_Grade: {
			type: DataTypes.FLOAT,
			allowNull: true
		}
	}, {
		tableName: 'Plant'
	});
};
