/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Check_Row', {
		Quantity: {
			type: DataTypes.INTEGER(8),
			allowNull: false
		},
		Grade: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		_Check_row_price: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		ID_order: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Order',
				key: 'ID_Order'
			}
		},
		ID_plant_in_pot: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Plant_in_Pot',
				key: 'ID_Plant_in_Pot'
			}
		}
	}, {
		tableName: 'Check_Row'
	});
};
