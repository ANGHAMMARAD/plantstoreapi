/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Plant_has_Wanted_Soil', {
		Plant_ID_Plant: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Plant',
				key: 'ID_Plant'
			}
		},
		Wanted_Soil_ID_Soil_Type: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Wanted_Soil',
				key: 'ID_Soil_Type'
			}
		}
	}, {
		tableName: 'Plant_has_Wanted_Soil'
	});
};
