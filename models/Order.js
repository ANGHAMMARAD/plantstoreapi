/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Order', {
		ID_Order: {
			type: DataTypes.INTEGER(10),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Date_Time: {
			type: DataTypes.DATE,
			allowNull: false
		},
		Country: {
			type: DataTypes.STRING(20),
			allowNull: false
		},
		Region: {
			type: DataTypes.STRING(20),
			allowNull: true
		},
		City: {
			type: DataTypes.STRING(20),
			allowNull: false
		},
		District: {
			type: DataTypes.STRING(30),
			allowNull: true
		},
		Street: {
			type: DataTypes.STRING(30),
			allowNull: false
		},
		House_Number: {
			type: DataTypes.STRING(10),
			allowNull: false
		},
		Flat_number: {
			type: DataTypes.INTEGER(10),
			allowNull: true
		},
		_Order_Price: {
			type: DataTypes.INTEGER(20),
			allowNull: false
		},
		ID_Client: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Client',
				key: 'ID_Client'
			}
		}
	}, {
		tableName: 'Order'
	});
};
