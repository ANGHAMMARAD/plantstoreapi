/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Wanted_Soil', {
		ID_Soil_Type: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Description: {
			type: DataTypes.STRING(20),
			allowNull: false
		}
	}, {
		tableName: 'Wanted_Soil'
	});
};
