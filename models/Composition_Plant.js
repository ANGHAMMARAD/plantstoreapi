/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Composition_Plant', {
		ID_Composition: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Composition',
				key: 'ID_Composition'
			}
		},
		ID_Plant: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Plant',
				key: 'ID_Plant'
			}
		}
	}, {
		tableName: 'Composition_Plant'
	});
};
