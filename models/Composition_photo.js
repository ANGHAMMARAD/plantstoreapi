/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Composition_photo', {
		ID_Comp_Photo: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Photo_Link: {
			type: DataTypes.STRING(200),
			allowNull: false
		},
		ID_Composition: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Composition',
				key: 'ID_Composition'
			}
		}
	}, {
		tableName: 'Composition_photo'
	});
};
