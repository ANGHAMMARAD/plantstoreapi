/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Plant_in_Pot', {
		ID_Plant_in_Pot: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		ID_Plant: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Plant',
				key: 'ID_Plant'
			}
		},
		ID_Pot: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'Pot_Size',
				key: 'ID_Pot'
			}
		},
		Quantity: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		Price: {
			type: DataTypes.INTEGER(20),
			allowNull: false
		},
		_Average_Grade: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		_Grades_Quantity: {
			type: DataTypes.INTEGER(6),
			allowNull: false
		},
		_Sold_Quantity: {
			type: DataTypes.INTEGER(6),
			allowNull: false
		},
	}, {
		tableName: 'Plant_in_Pot'
	});
};
