/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Composition', {
		ID_Composition: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		Name: {
			type: DataTypes.STRING(30),
			allowNull: false
		},
		Notes: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		_Average_Grade: {
			type: DataTypes.FLOAT,
			allowNull: true
		}
	}, {
		tableName: 'Composition'
	});
};
