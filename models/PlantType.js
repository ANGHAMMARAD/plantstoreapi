module.exports = function(sequelize, DataTypes){
    return sequelize.define('Plant_Type',{
        ID_Plant_Type: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
          },
          Description: {
            type: DataTypes.STRING(10),
            allowNull: false
          },
          
    })
  
}